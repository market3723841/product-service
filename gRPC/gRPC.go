package grpc

import (
	"gitlab.com/market3723841/product-service/config"
	"gitlab.com/market3723841/product-service/gRPC/client"
	"gitlab.com/market3723841/product-service/gRPC/service"
	product_service "gitlab.com/market3723841/product-service/genproto"
	"gitlab.com/market3723841/product-service/pkg/logger"
	"gitlab.com/market3723841/product-service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	product_service.RegisterProductServiceServer(grpcServer, service.NewProductService(cfg, log, strg, srvc))
	product_service.RegisterCategoryServiceServer(grpcServer, service.NewCategoryService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)

	return
}
